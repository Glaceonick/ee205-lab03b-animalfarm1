#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "cat.h"

#define MAX_CATS (100)
// @todo declare an array of struct Cat, call it catDB and it should have at least MAX_SPECIES records in it
enum Gender {MALE, FEMALE};
enum CatBreeds {MAIN_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};
enum Color {BLACK, WHITE, RED, BLUE, GREEN, PINK};

const char* Genders(enum Gender Gen)
{
	switch (Gen)
	{
		case MALE: return "Male";
		case FEMALE: return "Female";
	}
};

const char* Breeds(enum CatBreeds Bre)
{
	switch (Bre)
	{
		case MAIN_COON: return "Main Coon";
		case MANX: return "Manx";
		case SHORTHAIR: return "Short Hair";
		case PERSIAN: return "Persian";
		case SPHYNX: return "Sphynx";
	}
};

const char* Colors(enum Color Col)
{
	switch (Col)
	{
		case BLACK: return "Black";
		case WHITE: return "White";
		case RED: return "Red";
		case BLUE: return "Blue";
		case GREEN: return "Green";
		case PINK: return "Pink";
	}
};


struct CatDB
{

        char name [30];
        enum Gender gender;
        enum CatBreeds catBreeds;
        bool isFixed;
        float weight;
        enum Color collar1_color;
        enum Color collar2_color;
        long license;

};


/// Add Alice to the Cat catabase at position i.
///
/// @param int i Where in the catDB array that Alice the cat will go
///
/// @note This terrible style... we'd never hardcode this data, but it gets us started.
int main()
{
	const char* Boo;
	int i = 1;
        struct CatDB kitty[i];

        strcpy(kitty[i].name, "Alice");
        kitty[i].gender = FEMALE;
        kitty[i].catBreeds = MAIN_COON;
        kitty[i].isFixed = true;
        kitty[i].weight = 12.34;
        kitty[i].collar1_color = BLACK;
        kitty[i].collar2_color = RED;
        kitty[i].license = 12345;


        /*switch (kitty[i].isFixed)
        {
                case 1: Boo =  "True";
		case 0: Boo =  "False";
        }*/
	
	   // Here's a clue of what one printf() might look like...
   // printf ("    collar color 1 = [%s]\n", colorName( catDB[i].collar1_color ));
        printf("Cat name = [%s]\n", kitty[i].name);
        printf("gender = [%s]\n", Genders(kitty[i].gender));
        printf("breed = [%s]\n", Breeds(kitty[i].catBreeds));
        printf("isFixed = [%d]\n", kitty[i].isFixed);
        printf("weight = [%f]\n", kitty[i].weight);
        printf("collar color 1 = [%s]\n", Colors(kitty[i].collar1_color));
        printf("collar color 2 = [%s]\n", Colors(kitty[i].collar2_color));
        printf("license = [%ld]\n", kitty[i].license);





}
