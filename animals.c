///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author @Nicholas Tom <tom7@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @02_02_20221
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include "animals.h"

/// Decode the enum Color into strings for printf()
// char* colorName (enum Color color) {

   // @todo Map the enum Color to a string

//   return NULL; // We should never get here
// };


